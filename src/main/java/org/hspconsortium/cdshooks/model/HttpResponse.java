package org.hspconsortium.cdshooks.model;

public class HttpResponse {

    private String status;

    public HttpResponse() {
    }

    public HttpResponse(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public HttpResponse setStatus(String status) {
        this.status = status;
        return this;
    }
}
