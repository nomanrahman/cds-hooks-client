package org.hspconsortium.cdshooks.model;

import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.databind.JsonNode;

public class PrefetchInvocation {

    private HttpResponse response;

    private Object resource;

    public PrefetchInvocation() {
    }

    public PrefetchInvocation(HttpResponse response, Object resource) {
        this.response = response;
        this.resource = resource;
    }

    public HttpResponse getResponse() {
        return response;
    }

    public String getResponseCode() {
        return (response != null ? response.getStatus() : null);
    }

    public PrefetchInvocation setResponse(HttpResponse response) {
        this.response = response;
        return this;
    }

    @JsonRawValue
    public String getResource() {
        return resource != null ? resource.toString() : null;
    }

    public PrefetchInvocation setResource(JsonNode resource) {
        this.resource = resource;
        return this;
    }
}
