package org.hspconsortium.cdshooks.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Card {

    private String summary;

    private String detail;

    private String indicator;

    private Source source;

    private List<Suggestion> suggestions;

    private List<Link> links;

    public String getSummary() {
        return summary;
    }

    public Card setSummary(String summary) {
        this.summary = summary;
        return this;
    }

    public String getDetail() {
        return detail;
    }

    public Card setDetail(String detail) {
        this.detail = detail;
        return this;
    }

    public String getIndicator() {
        return indicator;
    }

    @JsonIgnore
    public Indicator asIndicator() {
        return Indicator.fromCode(indicator);
    }

    public Card setIndicator(Indicator indicator) {
        this.indicator = indicator.getCode();
        return this;
    }

    public Source getSource() {
        return source;
    }

    public Card setSource(Source source) {
        this.source = source;
        return this;
    }

    public Card addSuggestion(Suggestion suggestion) {
        if (suggestions == null) {
            suggestions = new ArrayList<>();
        }

        suggestions.add(suggestion);
        return this;
    }

    public Card addLink(Link link) {
        if (links == null) {
            links = new ArrayList<>();
        }

        links.add(link);
        return this;
    }

    public List<Link> getLinks() {
        return links;
    }

    public Card setLinks(List<Link> links) {
        this.links = links;
        return this;
    }
}
