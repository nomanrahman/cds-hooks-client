package org.hspconsortium.cdshooks.model;

import java.util.HashMap;
import java.util.Map;

public class ServiceInvocation {

    private String hook;

    private String hookInstance;

    private String fhirServer;

    private Object oauth;

    private String redirect;

    private String user;

    private String patient;

    private String encounter;

    private Object context;

    private Map<String, PrefetchInvocation> prefetch;

    public String getHook() {
        return hook;
    }

    public ServiceInvocation setHook(String hook) {
        this.hook = hook;
        return this;
    }

    public String getHookInstance() {
        return hookInstance;
    }

    public ServiceInvocation setHookInstance(String hookInstance) {
        this.hookInstance = hookInstance;
        return this;
    }

    public String getFhirServer() {
        return fhirServer;
    }

    public ServiceInvocation setFhirServer(String fhirServer) {
        this.fhirServer = fhirServer;
        return this;
    }

    public Object getOauth() {
        return oauth;
    }

    public ServiceInvocation setOauth(Object oauth) {
        this.oauth = oauth;
        return this;
    }

    public String getRedirect() {
        return redirect;
    }

    public ServiceInvocation setRedirect(String redirect) {
        this.redirect = redirect;
        return this;
    }

    public String getUser() {
        return user;
    }

    public ServiceInvocation setUser(String user) {
        this.user = user;
        return this;
    }

    public String getPatient() {
        return patient;
    }

    public ServiceInvocation setPatient(String patient) {
        this.patient = patient;
        return this;
    }

    public String getEncounter() {
        return encounter;
    }

    public ServiceInvocation setEncounter(String encounter) {
        this.encounter = encounter;
        return this;
    }

    public Object getContext() {
        return context;
    }

    public ServiceInvocation setContext(Object context) {
        this.context = context;
        return this;
    }

    public Map<String, PrefetchInvocation> getPrefetch() {
        return prefetch;
    }

    public ServiceInvocation setPrefetch(Map<String, PrefetchInvocation> prefetch) {
        this.prefetch = prefetch;
        return this;
    }

    public ServiceInvocation addPrefetch(String type, PrefetchInvocation prefetchInvocation) {
        if (prefetch == null) {
            prefetch = new HashMap<>();
        }

        prefetch.put(type, prefetchInvocation);
        return this;
    }
}
