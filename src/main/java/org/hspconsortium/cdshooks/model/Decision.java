package org.hspconsortium.cdshooks.model;

import java.util.ArrayList;
import java.util.List;

public class Decision {

    private List<String> create;

    private List<String> delete;

    public List<String> addCreate() {
        if (create == null) {
            create = new ArrayList<>();
        }

        return create;
    }

    public List<String> addDelete() {
        if (delete == null) {
            delete = new ArrayList<>();
        }

        return delete;
    }

}
