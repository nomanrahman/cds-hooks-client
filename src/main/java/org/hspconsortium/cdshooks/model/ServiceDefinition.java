package org.hspconsortium.cdshooks.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
abstract public class ServiceDefinition {

    private String id;

    private String hook;

    private String title;

    private String description;

    private Map<String, String> prefetch;

    public String getId() {
        return id;
    }

    public ServiceDefinition setId(String id) {
        this.id = id;
        return this;
    }

    public String getHook() {
        return hook;
    }

    public ServiceDefinition setHook(String hook) {
        this.hook = hook;
        return this;
    }

    public ServiceDefinition setHook(HookCatalog hookCatalog) {
        this.hook = hookCatalog.getValue();
        return this;
    }

    public String getTitle() {
        return title;
    }

    public ServiceDefinition setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public ServiceDefinition setDescription(String description) {
        this.description = description;
        return this;
    }

    public Map<String, String> getPrefetch() {
        return prefetch;
    }

    public ServiceDefinition setPrefetch(Map<String, String> prefetch) {
        this.prefetch = prefetch;
        return this;
    }

    public ServiceDefinition addPrefetch(String type, String fhirQuery) {
        if (this.prefetch == null) {
            this.prefetch = new HashMap<>();
        }

        this.prefetch.put(type, fhirQuery);

        return this;
    }

}
