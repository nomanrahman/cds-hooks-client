package org.hspconsortium.cdshooks.model;

public enum Indicator {

    Success("success"),
    Info("info"),
    Warning("warning"),
    HardStop("hard-stop");

    private String code;

    Indicator(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static Indicator fromCode(String code) {
        for (Indicator indicator : Indicator.values()) {
            if (indicator.getCode().equals(code)) {
                return indicator;
            }
        }
        throw new IllegalArgumentException("Unknown code: " + code);
    }
}
