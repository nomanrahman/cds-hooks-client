package org.hspconsortium.cdshooks.model;

public class Source {

    private String label;

    private String url;

    public Source() {
    }

    public Source(String label, String url) {
        this.label = label;
        this.url = url;
    }

    public String getLabel() {
        return label;
    }

    public Source setLabel(String label) {
        this.label = label;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public Source setUrl(String url) {
        this.url = url;
        return this;
    }

}
