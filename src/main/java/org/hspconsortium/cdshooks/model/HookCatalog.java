package org.hspconsortium.cdshooks.model;

public enum HookCatalog {

    MedicationPrescribe("medication-prescribe"),
    OrderReview("order-review"),
    PatientView("patient-view");

    String value;

    HookCatalog(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
