package org.hspconsortium.cdshooks.model;

import com.fasterxml.jackson.annotation.JsonValue;

public class Link {

    public enum Type {

        Absolute("absolute"),
        Smart("smart");

        private String code;

        Type(String code) {
            this.code = code;
        }

        @JsonValue
        public String getCode() {
            return code;
        }

        public static Type fromCode(String code) {
            for (Type type : Type.values()) {
                if (type.getCode().equals(code)) {
                    return type;
                }
            }
            throw new IllegalArgumentException("Unknown code: " + code);
        }
    }

    private String label;

    private String url;

    private Type type;

    public Link() {
    }

    public Link(String label, String url, Type type) {
        this.label = label;
        this.url = url;
        this.type = type;
    }

    public String getLabel() {
        return label;
    }

    public Link setLabel(String label) {
        this.label = label;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public Link setUrl(String url) {
        this.url = url;
        return this;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
