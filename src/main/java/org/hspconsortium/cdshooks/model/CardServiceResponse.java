package org.hspconsortium.cdshooks.model;

import org.apache.commons.lang3.Validate;

import java.util.ArrayList;
import java.util.List;

public class CardServiceResponse {

    private List<Card> cards = new ArrayList<>();

    private List<Decision> decisions = new ArrayList<>();

    public List<Card> getCards() {
        Validate.isTrue(decisions.isEmpty(), "Cannot add card to response after decisions have been added.");
        return cards;
    }

    public Card addCard(Card card) {
        Validate.isTrue(decisions.isEmpty(), "Cannot add card to response after decisions have been added.");
        cards.add(card);
        return card;
    }

    public List<Decision> getDecisions() {
        Validate.isTrue(cards == null, "Cannot add card to response after decisions have been added.");
        return decisions;
    }

    public Decision addDecision(Decision decision) {
        Validate.isTrue(cards == null, "Cannot add card to response after decisions have been added.");
        decisions.add(decision);
        return decision;
    }
}