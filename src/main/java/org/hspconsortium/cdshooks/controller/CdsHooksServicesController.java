package org.hspconsortium.cdshooks.controller;

import org.hspconsortium.cdshooks.model.ServiceResponse;
import org.hspconsortium.cdshooks.service.ExecutableService;
import org.hspconsortium.cdshooks.model.ServiceInvocation;
import org.hspconsortium.cdshooks.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/${hspc.cdsHooks.servicesUrl:cds-services}")
public class CdsHooksServicesController {

    @Autowired
    private ServiceRegistry serviceRegistry;

    @CrossOrigin(origins = "${hspc.cdsHooks.servicesUrl.allowOrigins:*}")
    @RequestMapping(path = "", method = RequestMethod.GET)
    public ServiceRegistry services() {
        return serviceRegistry;
    }

    @CrossOrigin(origins = "${hspc.cdsHooks.servicesInvokeUrl.allowOrigins:*}")
    @RequestMapping(path = "/{serviceId}", method = RequestMethod.POST)
    public ServiceResponse doService(@PathVariable String serviceId, @RequestBody ServiceInvocation serviceInvocation) {
        ExecutableService service = serviceRegistry.get(serviceId);

        return service.execute(serviceInvocation);
    }
}
